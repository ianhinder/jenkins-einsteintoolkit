Create a new data volume container for Jenkins configuration and build results:

    docker create --name jenkins-data ianhinder/jenkins-einsteintoolkit

Launch a new Jenkins container:

    docker run -d --publish 8080:8080 --name jenkins --volumes-from jenkins-data ianhinder/jenkins-einsteintoolkit

Visit the Jenkins instance on port 8080.  An EinsteinToolkit jenkins
job will have been created, and you should be able to start a build.
