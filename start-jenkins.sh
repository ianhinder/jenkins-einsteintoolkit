#!/bin/bash

chown -R jenkins /var/jenkins_home

su jenkins -c git config --global user.email "et@none.none"
su jenkins -c git config --global user.name "Einstein Toolkit"

su jenkins -c /usr/local/bin/jenkins.sh
